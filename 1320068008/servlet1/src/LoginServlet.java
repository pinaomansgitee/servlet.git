import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        response.setContentType("text/html; Charset=GB2312");
//        request.setCharacterEncoding("GB2312");


        PrintWriter out=response.getWriter();//获得输出流

        //获取参数值
        String strName = request.getParameter("UName");
        String strPwd = request.getParameter("PWD");

        //数据库连接

        Connection conn=null;
        Statement stmt;
        ResultSet rs;
        PreparedStatement pstmt;
        out.println("hello"+strName+strPwd);

        try {
//            Class.forName("com.microsoft.sqlserver.jdbc.SQlServerDriver");
//            conn = DriverManager.getConnection(
//                    "jdbc:sqlserver://localhost:1433;Databasename=login","sa","110110");
//            String sql = "select * from Tlogin where UName=? and UPwd=?";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setString(1,strName);
//            pstmt.setString(2,strPwd);
//            out.println(sql);
//            rs = pstmt.executeQuery();

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(
                    "jdbc:sqlserver://localhost:1433;Databasename=login","sa","110110");
            String  sql = "select * from Tlogin where UName=? and UPwd=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, strName);
            pstmt.setString(2, strPwd);
            System.out.println(sql);
            rs=pstmt.executeQuery();
//            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//            conn = DriverManager.getConnection(
//                    "jdbc:sqlserver://localhost:1433;Databasename=login","sa","110110");
//            stmt = conn.createStatement();
//            String sql = "select * from Tlogin where UName='"+strName+"' and UPwd='"+strPwd+"'";
//            out.println(sql);
//            rs = stmt.executeQuery(sql);

            if(rs.next())
            {
                out.println("welcome 用户："+strName+"<br>"+"密码："+strPwd+"<br>");
                Cookie cooUName=new Cookie("UName",strName);
                cooUName.setMaxAge(60*60*24*30);
                response.addCookie(cooUName);

                Cookie cooUPwd=new Cookie("UPwd",strPwd);
                cooUPwd.setMaxAge(60*60*24*30);
                response.addCookie(cooUPwd);

                HttpSession session = request.getSession(true);
//                session.setAttribute("UName",strName);
                ServletContext servletContext = getServletContext();
                Object num = servletContext.getAttribute("num");
                out.println("<br>");
                out.println("当前在线人数："+num);

                //response.sendRedirect("/html/login.html");
                //request.getRequestDispatcher("/html/login.html").forward(request,response);


            }else{
                out.println("用户名密码错误");
            }

            //资源释放
            rs.close();
            conn.close();
            pstmt.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
